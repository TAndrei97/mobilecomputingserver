package com.mobilecomputing.server.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mobilecomputing.server.model.Accomodation;

@Repository
public interface AccomodationRepository extends JpaRepository<Accomodation, Integer>{

	List<Accomodation> findAllByUserId(Integer id);

}
