package com.mobilecomputing.server.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobilecomputing.server.dto.UserDTO;
import com.mobilecomputing.server.mapper.UserMapper;
import com.mobilecomputing.server.model.User;
import com.mobilecomputing.server.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private UserRepository userRepository;

	public UserDTO register(UserDTO userDTO) {
		User user = userMapper.mapUserDto(userDTO);
		User savedUser = userRepository.save(user);
		return userMapper.mapUser(savedUser);
	}
	
	public UserDTO authenticate(UserDTO userDTO) {
		User user = userRepository.findTopByEmail(userDTO.getEmail());
		if(user == null) {
			return null;
		}
		
		UserDTO mappedUserDTO = userMapper.mapUser(user);
		if(mappedUserDTO.haveSameCredentials(userDTO)) {
			return mappedUserDTO;
		}
		
		return null;
		
	}

	public UserDTO get(Integer id) {
		Optional<User> optionalUser = userRepository.findById(id);
		if(optionalUser.isPresent()) {
			return userMapper.mapUser(optionalUser.get());
		}
		return null;
	}

}
