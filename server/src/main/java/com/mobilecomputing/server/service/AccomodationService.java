package com.mobilecomputing.server.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobilecomputing.server.dto.AccomodationDTO;
import com.mobilecomputing.server.mapper.AccomodationMapper;
import com.mobilecomputing.server.model.Accomodation;
import com.mobilecomputing.server.model.User;
import com.mobilecomputing.server.repository.AccomodationRepository;
import com.mobilecomputing.server.repository.UserRepository;

@Service
public class AccomodationService {

	@Autowired
	private AccomodationRepository accomodationRepository;

	@Autowired
	private UserRepository userRepository;
	
	
	@Autowired
	private AccomodationMapper accomodationMapper;
	
	public AccomodationDTO save(AccomodationDTO accomodationDTO, Integer userId) {
		
		Accomodation accomodation = accomodationMapper.mapAccomodationDTO(accomodationDTO);
		User user = userRepository.findById(userId).get();
		accomodation.setUser(user);
		Accomodation saved = accomodationRepository.save(accomodation);
		return accomodationMapper.mapAccomodation(saved);
	}

	public AccomodationDTO get(Integer id) {
		Optional<Accomodation> optionalAccomodation = accomodationRepository.findById(id);
		if(optionalAccomodation.isPresent()) {
			return accomodationMapper.mapAccomodation(optionalAccomodation.get());
		}
		return null;
	}

	public List<AccomodationDTO> findAllForUser(Integer id) {
		List<Accomodation> allByUserId = accomodationRepository.findAllByUserId(id);
		
		return accomodationMapper.mapAccomodations(allByUserId);
	}

	
}
