package com.mobilecomputing.server.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties
public class AccomodationDTO {

	private Integer id;
	private Integer userId;
	private String name;
	private String description;
	private String phone;
	private String address;

}
