package com.mobilecomputing.server.controller;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mobilecomputing.server.dto.UserDTO;
import com.mobilecomputing.server.service.UserService;
import com.mobilecomputing.server.util.UriUtil;

@RestController
@RequestMapping(value="/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/login")
	public ResponseEntity<UserDTO> loginUser(@RequestBody UserDTO userDTO) {
		UserDTO authUser = userService.authenticate(userDTO);
		if(authUser == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(authUser);
	}
	
	@PostMapping("/register")
	public ResponseEntity<UserDTO> register(@RequestBody UserDTO userDTO) {
		UserDTO registeredUser = userService.register(userDTO);
		return ResponseEntity.created(UriUtil.buildUri("/{id}", registeredUser.getId())).body(registeredUser);
		
	}
	
	@GetMapping("/{id}")
	public UserDTO register(@PathVariable Integer id) {
		UserDTO user = userService.get(id);
		return user;
	}
	
	
	@PostMapping("/upload")
	public String fileUpload(@RequestParam("file") MultipartFile file) {
	    try {
	        // Get the file and save it somewhere
	        byte[] bytes = file.getBytes();

	        //save file in server - you may need an another scenario
	        Path path = Paths.get("/" + file.getOriginalFilename());
	        Files.write(path, bytes);

	    } catch (IOException e) {
	        e.printStackTrace();
	    }

	    //redirect to an another url end point 
	    return "redirect:/upload-status";
	}
}
