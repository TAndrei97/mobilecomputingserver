package com.mobilecomputing.server.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mobilecomputing.server.dto.AccomodationDTO;
import com.mobilecomputing.server.dto.UserDTO;
import com.mobilecomputing.server.service.AccomodationService;
import com.mobilecomputing.server.util.UriUtil;

@RestController
@RequestMapping(value="/accomodation")
public class AccomodationController {
	
	@Autowired
	private AccomodationService accomodationService;
	
	@PostMapping("/{id}")
	public ResponseEntity<AccomodationDTO> save(@PathVariable Integer id, @RequestBody AccomodationDTO accomodationDTO) {
		AccomodationDTO savedAccomodation = accomodationService.save(accomodationDTO,id);
		return ResponseEntity.created(UriUtil.buildUri("/{id}", savedAccomodation.getId())).body(savedAccomodation);
		
	}
	
	@GetMapping("/{id}")
	public AccomodationDTO get(@PathVariable Integer id) {
		AccomodationDTO accomodation = accomodationService.get(id);
		return accomodation;
	}
	
	@GetMapping("/all/{id}")
	public List<AccomodationDTO> getAll(@PathVariable Integer id) {
		List<AccomodationDTO> accomodation = accomodationService.findAllForUser(id);
		return accomodation;
	}
}
