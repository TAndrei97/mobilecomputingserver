package com.mobilecomputing.server.mapper;

import org.springframework.stereotype.Component;

import com.mobilecomputing.server.dto.UserDTO;
import com.mobilecomputing.server.model.User;

@Component
public class UserMapper {

	public User mapUserDto(UserDTO userDTO) {
		User user = new User();

		user.setId(userDTO.getId());
		user.setEmail(userDTO.getEmail());
		user.setPassword(userDTO.getPassword());

		return user;
	}
	
	public UserDTO mapUser(User user) {
		return new UserDTO(user.getId(), user.getEmail(), user.getPassword());
	}

}
