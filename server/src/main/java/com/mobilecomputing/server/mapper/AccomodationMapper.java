package com.mobilecomputing.server.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.mobilecomputing.server.dto.AccomodationDTO;
import com.mobilecomputing.server.model.Accomodation;

@Component
public class AccomodationMapper {

	public Accomodation mapAccomodationDTO(AccomodationDTO accomodationDTO) {
		Accomodation accomodation = new Accomodation();
		accomodation.setName(accomodationDTO.getName());
		accomodation.setDescription(accomodationDTO.getDescription());
		accomodation.setPhone(accomodationDTO.getPhone());
		accomodation.setAddress(accomodationDTO.getAddress());

		return accomodation;
	}

	public AccomodationDTO mapAccomodation(Accomodation saved) {
		AccomodationDTO dto = new AccomodationDTO(saved.getId(),saved.getUser().getId(), saved.getName(), saved.getDescription(), saved.getPhone(), saved.getAddress());
		return dto ;
//		return null;
	}

	public List<AccomodationDTO> mapAccomodations(List<Accomodation> allByUserId) {
		List<AccomodationDTO> accomodationsDTO = new ArrayList<>();
		
		for(Accomodation accomodation : allByUserId) {
			accomodationsDTO.add(mapAccomodation(accomodation));
		}
		return accomodationsDTO;
	}

}
